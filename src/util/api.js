/**
 * @author Édson Fischborn
 */
export class BaseApi {
  constructor(baseUrl = "", headers = {}) {
    this.headers = headers;
    this.baseUrl = this._normalizeBaseUrl(baseUrl);
  }

  /**
   *
   * @param {string} baseUrl
   * @returns Trim str -> www.example.com/ -> www.example.com
   */
  _normalizeBaseUrl = (baseUrl = "") => {
    return String(baseUrl).replace(/[/]$/, "");
  };

  /**
   *
   * @param {string} path
   * @returns Trim str -> /path/path2 -> /path
   */
  _normalizePath = (path = "") => {
    return path.startsWith("/") ? path.substring(1) : path;
  };

  /**
   *
   * @param {string} path
   * @param {'POST' | 'GET' | 'PUT'} method
   * @returns Request
   */
  _getRequest = (path = "", method = "GET") => {
    const url = `${this.baseUrl}/${this._normalizePath(path)}`;

    return new Request(url, {
      method,
      headers: this.headers,
    });
  };

  /**
   *
   * @param {string} path
   * @returns Promise
   */
  get = (path = "") => fetch(this._getRequest(path));

  /*  post = (path = "") => null;
  put = (path = "") => null;
  delete = (path = "") => null; */
}
