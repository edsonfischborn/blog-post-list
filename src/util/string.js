export const truncateStr = (str = "", length = 999) => {
  return str.length > length ? `${str.substring(0, length)}…` : str;
};
