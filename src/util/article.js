export const isValidCategoryName = (category) =>
  category && typeof category === "string" && !category.startsWith("/");
