import React from "react";

// Styles
import S from "./styles.module.css";

export const Spinner = () => {
  return (
    <div className={S.SpinnerContainer}>
      <div className={S.SpinnerCircle} />
      <span className={S.SpinnerMessage}>Aguarde...</span>
    </div>
  );
};
