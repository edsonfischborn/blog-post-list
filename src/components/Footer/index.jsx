import React from "react";

// Styles
import S from "./styles.module.css";

/**
 * @author Édson Fischborn
 * @returns Footer
 */
export const Footer = () => {
  return (
    <footer className={S.Footer}>
      <a
        className={S.FooterLink}
        href="https://github.com/edsonfischborn"
        target="_blank"
        rel="noreferrer"
      >
        Édson Fischborn
      </a>
    </footer>
  );
};
