import { ArticleProps } from "../../types";

export type ArticleCardProps = ArticleProps;
