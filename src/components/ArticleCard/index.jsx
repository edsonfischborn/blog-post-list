/**
 * @typedef { import('./ArticleCard').ArticleCardProps } ArticleCardProps
 */

import React, { useCallback } from "react";
import { useEffect } from "react";
import { useMemo, useState } from "react";
import { truncateStr } from "../../util";

// Styles
import S from "./styles.module.css";

/**
 * @param {ArticleCardProps} props
 * @author Édson Fischborn
 * @returns ArticleCard
 */
export const ArticleCard = (props) => {
  const {
    thumbImage = "",
    thumbImageAltText = "",
    title = "",
    description = "",
    profileThumbImage = "",
    postDate = new Date(),
    profileName = "",
  } = props;

  const [isOpenShareOptions, setIsOpenShareOptions] = useState(false);

  const handleShareBtn = useCallback((e) => {
    e.stopPropagation();
    setIsOpenShareOptions((prevState) => !prevState);
  }, []);

  useEffect(() => {
    window.addEventListener("click", () => setIsOpenShareOptions(false));
  }, []);

  const parsedPostDate = useMemo(() => {
    const options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };

    return new Date(postDate).toLocaleString("pt-BR", options);
  }, [postDate]);

  const parsedTitle = useMemo(() => truncateStr(title, 80), [title]);
  const parsedDescription = useMemo(
    () => truncateStr(description, 190),
    [description]
  );

  return (
    <article className={S.Card}>
      <div className={S.CardThumb}>
        <img
          className={S.CardThumbImage}
          src={thumbImage}
          alt={thumbImageAltText}
        />
      </div>
      <aside className={S.CardContent}>
        <div>
          <h2 className={S.CardContentTitle}>{parsedTitle}</h2>
          <p className={S.CardContentDescription}>{parsedDescription}</p>
        </div>

        <div className={S.CardContentFooter}>
          <div className={S.CardContentFooterProfile}>
            <img
              className={S.CardContentFooterProfileImage}
              src={profileThumbImage}
              alt={profileName}
            />
            <div className={S.CardContentFooterProfileUser}>
              <strong className={S.CardContentFooterProfileUserName}>
                {profileName}
              </strong>
              <span className={S.CardContentFooterProfilePostDate}>
                {parsedPostDate}
              </span>
            </div>
          </div>

          {isOpenShareOptions && (
            <div className={S.CardContentFooterShareOptions}>
              <span className={S.CardContentFooterShareOptionsTitle}>
                share
              </span>
              <a
                href="http://fb.com"
                target="_blank"
                rel="noreferrer"
                className={`${S.CardContentFooterShareOptionsLink} ${S.FacebookHoverColor}`}
              >
                <span className={S.Sronly}>share with fb</span>
                <i className="fab fa-facebook-square"></i>
              </a>
              <a
                href="http://twitter.com"
                target="_blank"
                rel="noreferrer"
                className={`${S.CardContentFooterShareOptionsLink} ${S.TwitterHoverColor}`}
              >
                <i className="fab fa-twitter"></i>
                <span className={S.Sronly}>share with tw</span>
              </a>
              <a
                href="http://pinterest.com"
                target="_blank"
                rel="noreferrer"
                className={`${S.CardContentFooterShareOptionsLink} ${S.PinterestHoverColor}`}
              >
                <i className="fab fa-pinterest"></i>
                <span className={S.Sronly}>share with pinterest</span>
              </a>
            </div>
          )}

          <button
            className={
              isOpenShareOptions
                ? `${S.CardContentFooterShareBtn} ${S.CardContentFooterShareBtnActive}`
                : S.CardContentFooterShareBtn
            }
            onClick={handleShareBtn}
          >
            <span className={S.Sronly}>share</span>
            <i className="fas fa-share"></i>
          </button>
        </div>
      </aside>
    </article>
  );
};
