import React from "react";

// App css
import "./app.css";

// Pages
import { Routes } from "./routes";

/**
 * @author Édson Fischborn
 * @returns App
 */
export const App = () => <Routes />;
