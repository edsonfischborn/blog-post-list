export interface ArticleProps {
  thumbImage: string;
  thumbImageAltText: string;
  title: string;
  description: string;
  postDate: Date;
  profileThumbImage: string;
  profileName: string;
}
