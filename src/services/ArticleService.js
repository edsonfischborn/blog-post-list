/**
 * @typedef { import('../types').ArticleProps } ArticleProps
 */
import { BaseApi } from "../util";

// Constants
import { ARTICLE_API_BASE_URL } from "../constants";

// Util
import { isValidCategoryName } from "../util";

/**
 * @author Édson Fischborn
 */
class ArticleService extends BaseApi {
  constructor() {
    super(ARTICLE_API_BASE_URL);
  }

  /**
   * @returns {{ok: boolean, data: ArticleProps[] | {} }}
   */
  getAll = async () => {
    try {
      const articlesRequest = await this.get("/postagem");

      const data = await articlesRequest.json();

      return { ok: true, data };
    } catch (ex) {
      console.error(
        "🚀 ~ file: ArticleService.js ~ line 29 ~ ArticleService ~ getArticles= ~ ex",
        ex
      );

      return { ok: false, data: {} };
    }
  };

  /**
   * @returns {{ok: boolean, data: ArticleProps[] | {} }}
   */
  getByCategory = async (category = "") => {
    if (!isValidCategoryName(category)) {
      console.warn(
        "🚀 ~ file: ArticleService.js ~ line 43 ~ ArticleService ~ getByCategory invalid category"
      );

      return { ok: false, data: {} };
    }

    try {
      const articlesRequest = await this.get(`/postagem/categoria/${category}`);

      const data = await articlesRequest.json();

      return { ok: true, data };
    } catch (ex) {
      console.error(
        "🚀 ~ file: ArticleService.js ~ line 55 ~ ArticleService ~ getByCategory= ~ ex",
        ex
      );

      return { ok: false, data: {} };
    }
  };
}

export const articleService = new ArticleService();
