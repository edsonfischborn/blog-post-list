export const articlesMock = [
  {
    thumbImage: "/images/post-1.jpg",
    thumbImageAltText: "Fotografias",
    title:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum dolores magnam, asperiores labore eum nulla eius sint cum explicabo totam iure",
    profileThumbImage: "/images/profile-1.jpg",
    profileName: "Maria Silva",
    postDate: Date.now(),
  },
  {
    thumbImage: "/images/post-2.jpg",
    thumbImageAltText: "Fotografias",
    title: "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum dolores magnam",
    profileThumbImage: "/images/profile-2.jpg",
    profileName: "Paula Ramos",
    postDate: Date.now(),
  },
  {
    thumbImage: "/images/post-3.jpg",
    thumbImageAltText: "Fotografias",
    title: "Lorem, ipsum dolor sit amet consectetur",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum dolores magnam, asperiores labore eum nulla eius sint cum explicabo totam iure",
    profileThumbImage: "/images/profile-3.jpg",
    profileName: "Rodrigo Silveira",
    postDate: Date.now(),
  },
  {
    thumbImage: "/images/post-4.jpg",
    thumbImageAltText: "Fotografias",
    title:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum dolores magnam, asperiores labore eum nulla eius sint cum explicabo totam iure Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum",
    profileThumbImage: "/images/profile-4.jpg",
    profileName: "Lucas Oliveira",
    postDate: Date.now(),
  },
  {
    thumbImage: "/images/post-5.jpg",
    thumbImageAltText: "Fotografias",
    title:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum",
    description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
    profileThumbImage: "/images/profile-1.jpg",
    profileName: "Maria Silva",
    postDate: Date.now(),
  },
  {
    thumbImage: "/images/post-6.jpg",
    thumbImageAltText: "Fotografias",
    title:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum dolores magnam",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis laborum ipsum dolores magnam, asperiores labore eum nulla eius sint cum explicabo totam iure",
    profileThumbImage: "/images/profile-2.jpg",
    profileName: "Debora Pacheco",
    postDate: Date.now(),
  },
];

export const articlesCategories = ["web", "games"];
