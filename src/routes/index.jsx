import React from "react";
import { BrowserRouter, Routes as DOMRoutes, Route } from "react-router-dom";

// Pages
import { Home } from "../pages";

export const Routes = () => {
  return (
    <BrowserRouter>
      <DOMRoutes>
        <Route path="*" element={<Home />} />
      </DOMRoutes>
    </BrowserRouter>
  );
};
