import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSearchParams } from "react-router-dom";

// Styles
import S from "./styles.module.css";

// Components
import { ArticleCard, Footer, Spinner } from "../../components";
import { HomeHeader } from "./components";

// Services
import { articleService } from "../../services";

// Util
import { isValidCategoryName } from "../../util";

// Constants
import {
  ARTICLE_API_BASE_URL,
  CATEGORY_URL_SEARCH_PARAM,
} from "../../constants";

/**
 * @author Édson Fischborn
 * @returns HomePage
 */
export const Home = () => {
  const [state, setState] = useState({ isLoading: false, articles: [] });
  const [searchParams] = useSearchParams();

  const selectedCategory = useMemo(() => {
    return searchParams.get(CATEGORY_URL_SEARCH_PARAM);
  }, [searchParams]);

  const beforeFetchArticles = useCallback(() => {
    setState((prev) => ({ ...prev, isLoading: true }));
  }, []);

  const afterFetchArticles = useCallback((ok, data) => {
    if (!ok) {
      return setState((prev) => ({
        ...prev,
        isLoading: false,
      }));
    }

    const articlesWithFullImagePath = data.map((article) => ({
      ...article,
      thumbImage: `${ARTICLE_API_BASE_URL}${article.thumbImage}`,
      profileThumbImage: `${ARTICLE_API_BASE_URL}${article.profileThumbImage}`,
    }));

    setTimeout(() => {
      setState((prev) => ({
        ...prev,
        isLoading: false,
        articles: articlesWithFullImagePath,
      }));
    }, 2000);
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      beforeFetchArticles();

      if (isValidCategoryName(selectedCategory)) {
        const { ok, data } = await articleService.getByCategory(
          selectedCategory
        );

        afterFetchArticles(ok, data);
      } else {
        const { ok, data } = await articleService.getAll();

        afterFetchArticles(ok, data);
      }
    };

    fetchData();
  }, [afterFetchArticles, beforeFetchArticles, selectedCategory, setState]);

  return (
    <>
      <HomeHeader defaultDropdownItem="todas" />

      <main className={state.isLoading ? S.Conainer__Loading : S.Container}>
        {state.isLoading && <Spinner />}
        {!state.isLoading &&
          state.articles.map((props, index) => (
            <div key={index} className={S.PostWrapper}>
              <ArticleCard {...props} />
            </div>
          ))}
        {!state.isLoading && state.articles.length === 0 && (
          <span>Nenhum conteúdo para mostrar!</span>
        )}
      </main>

      <Footer />
    </>
  );
};
