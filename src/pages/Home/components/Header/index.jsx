import React, { useState, useCallback, useEffect, useMemo } from "react";
import { useSearchParams } from "react-router-dom";

// Styles
import S from "./styles.module.css";

// Mocks
import { articlesCategories } from "../../../../mocks";

// Constants
import { CATEGORY_URL_SEARCH_PARAM } from "../../../../constants";

// Util
import { isValidCategoryName } from "../../../../util";

/**
 * @author Édson Fischborn
 * @param {HeaderProps} props
 * @returns HomeHeader
 */
export const HomeHeader = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [isOpenDropDown, setIsOpenDropdown] = useState(false);

  const handleDropwdown = useCallback((e) => {
    e.stopPropagation();
    setIsOpenDropdown((prevState) => !prevState);
  }, []);

  useEffect(() => {
    window.addEventListener("click", () => setIsOpenDropdown(false));
  }, []);

  const ALL_CATEGORIES_NAME = useMemo(() => "todas", []);

  const onSelectDropdownOption = useCallback(
    (evt) => {
      const category = evt.target.innerText;

      if (category === ALL_CATEGORIES_NAME) {
        return setSearchParams({});
      }

      setSearchParams({ category });
    },
    [ALL_CATEGORIES_NAME, setSearchParams]
  );

  const selectedCategory = useMemo(() => {
    const cat = searchParams.get(CATEGORY_URL_SEARCH_PARAM);

    return isValidCategoryName(cat) ? cat : ALL_CATEGORIES_NAME;
  }, [ALL_CATEGORIES_NAME, searchParams]);

  const categories = useMemo(
    () => [...articlesCategories, ALL_CATEGORIES_NAME],
    [ALL_CATEGORIES_NAME]
  );

  return (
    <header className={S.Header}>
      <h1>Ultimas postagens</h1>
      <div className={S.HeaderDropdown}>
        <button className={S.HeaderDropdownButton} onClick={handleDropwdown}>
          <i className={`${S.HeaderDropdownButtonIcon} fa-solid fa-filter`}></i>
          <span>Categorias</span>
        </button>

        {isOpenDropDown && (
          <div className={S.HeaderDropdownContent}>
            <ul className={S.HeaderDropdownContentList}>
              {categories.map((cat, index) => {
                return (
                  <li key={index}>
                    <button
                      className={S.HeaderDropdownContentListItem}
                      onClick={onSelectDropdownOption}
                    >
                      {cat}
                    </button>

                    {cat === selectedCategory && (
                      <i
                        className={`${S.HeaderDropdownContentListItemSelectedIcon} fa-solid fa-check`}
                      ></i>
                    )}
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
    </header>
  );
};
